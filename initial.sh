#!/bin/bash
set -e

# init pacman
pacman-key --init
pacman-key --populate archlinuxarm
pacman -ySu sudo uboot-tools bash-completion

# set locale
echo "en_US.UTF-8 UTF-8" >> /etc/locale.gen
echo "ru_RU.UTF-8 UTF-8" >> /etc/locale.gen
echo "LANG=en_US.UTF-8" >> /etc/locale.conf
echo "LANGUAGE=en_US" >> /etc/locale.conf
echo "LC_ALL=C" >> /etc/locale.conf
locale-gen

# sudo and enable sshd
sed 's/# %wheel/%wheel/' -i /etc/sudoers
systemctl enable sshd.service
