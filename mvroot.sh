#!/bin/bash
set -e

if [ -z $1 ]; 
then
    echo "Not set disk name e.g. sda"
    echo "check disk name: fdisk -l"
    exit 1
fi

pacman -ySu parted

export target_disc=/dev/$1

# preparation of sata-device
sudo parted -s ${target_disc} mklabel msdos
sudo parted -a optimal -- ${target_disc} mkpart primary 2048s 100%
sudo dd if=/dev/mmcblk0p2 of=${target_disc}1
sudo e2fsck -y -f ${target_disc}1
sudo resize2fs ${target_disc}1
export target_partuuid=$(sudo blkid ${target_disc}1 | awk '{print $2}')

# install boot-system 
cat > boot.cmd.sata <<EOF
part uuid \${devtype} \${devnum}:\${bootpart} uuid
setenv bootargs console=\${console} root=/dev/${target_disk}2 rw rootwait

if load \${devtype} \${devnum}:\${bootpart} \${kernel_addr_r} zImage; then
  if load \${devtype} \${devnum}:\${bootpart} \${fdt_addr_r} dtbs/\${fdtfile}; then
    if load \${devtype} \${devnum}:\${bootpart} \${ramdisk_addr_r} initramfs-linux.img; then
      bootz \${kernel_addr_r} \${ramdisk_addr_r}:\${filesize} \${fdt_addr_r};
    else
      bootz \${kernel_addr_r} - \${fdt_addr_r};
    fi;
  fi;
fi

if load \${devtype} \${devnum}:\${bootpart} 0x48000000 uImage; then
  if load \${devtype} \${devnum}:\${bootpart} 0x43000000 script.bin; then
    setenv bootm_boot_mode sec;
    bootm 0x48000000;
  fi;
fi
EOF

sudo mv /boot/boot.scr /boot/boot.scr.sd-card
sudo mkimage -A arm -O linux -T script -C none -a 0 -e 0 -n "BananPI boot script" -d boot.cmd.sata /boot/boot.scr

