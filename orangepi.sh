#!/bin/bash
set -e

if [ -z $1 ]; 
then
    echo "Not set disk name e.g. sdb"
    exit 1
fi

export target_disc=/dev/$1

# install depends
sudo pacman --needed -S uboot-tools wget swig arm-none-eabi-gcc dtc git
# clearing the sd card
sudo dd if=/dev/zero of=${target_disc} bs=1M count=16

# create partitions
sudo parted -s ${target_disc} mklabel msdos
sudo parted -a optimal -- ${target_disc} mkpart primary 2048s 150M
sudo parted -a optimal -- ${target_disc} mkpart primary 150M 100%

# create fs 
sudo mkfs.ext4 -F -O ^metadata_csum,^64bit ${target_disc}1
sudo mkfs.ext4 -F -O ^metadata_csum,^64bit ${target_disc}2

# get uuid 
export target_uuid1=$(sudo blkid ${target_disc}1 | awk '{print $2}')
export target_uuid2=$(sudo blkid ${target_disc}2 | awk '{print $2}')
export target_partuuid=$(sudo blkid ${target_disc}2 | awk '{print $2}')

# write the images
mkdir -p OrangePi
cd OrangePi
mkdir -p root
mkdir -p boot
sudo mount ${target_disc}1 boot
sudo mount ${target_disc}2 root
curl -L -O http://os.archlinuxarm.org/os/ArchLinuxARM-aarch64-latest.tar.gz
sudo bsdtar -xpf ArchLinuxARM-aarch64-latest.tar.gz -C root
sudo mv root/boot/* boot/.
echo "${target_uuid2}       /               ext4            rw,relatime     0 1" | sudo tee -a root/etc/fstab
echo "${target_uuid1}       /boot           ext4            rw,relatime     0 2" | sudo tee -a root/etc/fstab

# install boot system
cat > boot.cmd.sd-card << EOF
part uuid \${devtype} \${devnum}:\${bootpart} uuid
setenv bootargs console=\${console} root=${target_partuuid} rw rootwait

if load \${devtype} \${devnum}:\${bootpart} \${kernel_addr_r} zImage; then
  if load \${devtype} \${devnum}:\${bootpart} \${fdt_addr_r} dtbs/\${fdtfile}; then
    if load \${devtype} \${devnum}:\${bootpart} \${ramdisk_addr_r} initramfs-linux.img; then
      bootz \${kernel_addr_r} \${ramdisk_addr_r}:\${filesize} \${fdt_addr_r};
    else
      bootz \${kernel_addr_r} - \${fdt_addr_r};
    fi;
  fi;
fi

if load \${devtype} \${devnum}:\${bootpart} 0x48000000 uImage; then
  if load \${devtype} \${devnum}:\${bootpart} 0x43000000 script.bin; then
    setenv bootm_boot_mode sec;
    bootm 0x48000000;
  fi;
fi
EOF

sudo mkimage -A arm -O linux -T script -C none -a 0 -e 0 -n "OrangePI boot script" -d boot.cmd.sd-card boot/boot.scr
sudo cp ../initial.sh root/root/
sudo umount boot
sudo umount root

# install u-boot
git clone git://git.denx.de/u-boot.git
cd u-boot
git checkout tags/v2021.04
make -j4 ARCH=arm CROSS_COMPILE=arm-none-eabi- bananapi_m1_plus_defconfig
make -j4 ARCH=arm CROSS_COMPILE=arm-none-eabi-
sudo dd if=u-boot-sunxi-with-spl.bin of=${target_disc} bs=1024 seek=8 && sync
