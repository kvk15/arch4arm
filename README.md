# Скрипты создания SD для одноплатников ARM
* **bananapi.sh** - создаёт SD card для Banana PI M1+ [Banana Pi M1+](http://www.banana-pi.org/m1p.html). За основу взят документ [installation archlinux](https://github.com/sosyco/bananapi/blob/master/installation_english.md)

## Использование
Смотрим каким диском у нас появилась SD которую будем использовать
```bash
fdisk -l
```
Запускаем нужный нам скрипт
```bash
bananapi.sh sdb
```
По завершению скрипта на SD карте /dev/sdb будет установлен последний [ALARM Project](https://archlinuxarm.org/)

## Скрипт первичной настройки
* **initial.sh**
настраивает локали, pacman, sudo.

## Скрипт копирования / на SSD(USB)
Все действия производим на SBC. Копируем на SCB скрипт:
```bash
scp mvroot.sh IP-ADDRESS-SBC
``` 
Смотрим каким диском у нас появился SSD который будем использовать
```bash
fdisk -l
```
Запускаем скрипт
```bash
mvroot.sh sda
```
